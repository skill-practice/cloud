package com.home.practice.one.controller

import com.home.practice.one.config.AppConfigProperties
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/configs")
class ConfigControllerApi(
        private val appConfigProperties: AppConfigProperties
) {

    @GetMapping("/description")
    fun getConfigName(): String  = appConfigProperties.description

    @PostMapping("/show-text")
    fun getText(
            @RequestBody data: Map<String, Any>
    ): Map<String, Any>{
        return data
    }

}
