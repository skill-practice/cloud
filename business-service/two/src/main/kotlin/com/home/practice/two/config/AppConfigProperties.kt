package com.home.practice.two.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "app.config")
data class AppConfigProperties(
        var description: String
)
