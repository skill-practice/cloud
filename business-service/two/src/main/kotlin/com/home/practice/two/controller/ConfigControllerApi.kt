package com.home.practice.two.controller

import com.home.practice.two.config.AppConfigProperties
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/configs")
class ConfigControllerApi(
        private val appConfigProperties: AppConfigProperties
) {
    @Value("\${app.config.description}")
    lateinit var description: String

    @GetMapping("/description")
    fun getConfigName(): String {
        return "'${appConfigProperties.description}'<br/>'$description'"
    }

}
